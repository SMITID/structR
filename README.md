# SMITIDstruct : a R package for data storage, manipulation and indexation for SMITID (Statistical Methods for Inferring Transmissions of Infectious Diseases from deep sequencing data).

This R package contains host and viral populations data structures according times, spaces and genenomics data.

Project page : [SMITID](https://informatique-mia.inra.fr/biosp/anr-smitid-project)


## Install

Install _Biostring_ depuis bioconductor :  
```
source("https://bioconductor.org/biocLite.R")
biocLite("Biostrings")
```  

Install SMITIDstruct :  
```
Rscript -e "roxygen2::roxygenize('.', roclets=c('rd', 'collate', 'namespace'))"
R CMD build .
R CMD INSTALL SMITIDstruct_*.tar.gz
```

## Documentation

Soon

## Demo

Check demo.SMITIDstruct.run() method for example.

## Author

Jean-François Rey <jean-francois.rey at inra.fr>

